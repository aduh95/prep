<?php
/**
* Autoload function for PREP's classes
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Prep.class
* @since        4.2.0
*
*/
if(!defined('PHP_VERSION_ID') || PHP_VERSION_ID < 50600)
	exit('PREP is not compatible with your current version of PHP, it requires PHP 5.6 or higher'."\n");


spl_autoload_register(function ($class) {
    if ($class==='Prep') {
        require 'Prep.class.php';
    } elseif (substr($class, 0, 5)==='prep\\') {
        $exception = false;
        $ext = 'class';

        $class = substr($class, 5);
        $dir = __DIR__.DIRECTORY_SEPARATOR.'prep_files';
        $exception+= !is_dir($dir);

        if (substr($class, -9)==='Exception') {
            $dir.= DIRECTORY_SEPARATOR.'exceptions';
        } elseif (substr($class, -9)==='Interface') {
            $dir.= DIRECTORY_SEPARATOR.'interfaces';
            $ext = 'interface';
        } elseif (substr($class, -5)==='Trait') {
            $dir.= DIRECTORY_SEPARATOR.'traits';
            $ext = 'trait';
        }

        $file = $dir.DIRECTORY_SEPARATOR.$class.'.'.$ext.'.php';
        $exception+= !is_file($file);

        if($class==='Exception') {
            require_once $file;
        } elseif ($exception) {
            throw new prep\Exception('Missing file or directory "'.$file.'". Unable to load class prep\\'.$class);
        } else {
            include $file;
        }
    }
}, true, true);
