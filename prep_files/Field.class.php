<?php
/**
* Declare the class Field, which uses trait  databaseMethodsTrait
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Field.class
* @since        3.0.2
*
*/
namespace prep;

/**
 * Create a field for prep\Select and prep\Query
 */
class Field implements FieldInterface
{
    use  prepMethodsTrait ,databaseMethodsTrait ,conversionTrait;

    /**
     * @var \prep\ColumnInterface $column The column of this field
     */
    protected $column;

    /**
     * Object constructor
     *
     * Method definitions:
     *
     * - Field::__construct(~Table $table, ~Column $column[, mixed $value[, ~string $alias[, ~string $fctColumn
     *                  [, ~string fctValue[, ~string $orderBy[,,, array $defaultSettings]]]]]])
     * - Field::__construct(array $arguments)
     * - Condition::__construct(~Table $table, ~Column $column[, mixed $value[, ~string $alias[, ~string $fctColumn
     *                      [, ~string fctValue[, ~string $orderBy[, bool|~string $operator[, ~string $logic_link
     *                      [, array $defaultSettings]]]]]]]])
     * - Condition::__construct(array $arguments)
     *
     * Parameters:
     *
     * 0. table           => ~Table  The Table object of the field's table
     * 1. column          =>   mixed Name of the column, or an array containing the arguments of fctColumn
     * 2. value           =>   mixed
     * 3. alias           => ~string Alias of the column
     * 4. fctColumn       => ~string
     * 5. fctValue        => ~string
     * 6. orderBy        => ~string optional "ASC" or "DESC" for ORDER BY
     * 7. operator        => bool|~string (only for Condition)
     * 8. logicLink      => ~string "AND" or "OR", default is "AND" (only for Condition)
     * 9. defaultSettings => array
     *
     */
    public function __construct()
    {
        $defaultSettings = ArgumentException::checkArgs(
            [[9=>'defaultSettings'], [], [9=>[null, 2=>[]]]],
            func_get_args()
        )['defaultSettings'];

        $this->param = ArgumentException::checkArgs(
            [
                ['table', 'column', 'value', 'alias', 'fctColumn', 'fctValue', 'orderBy', 'operator', 'logicLink',],
                ['column'], // required args
                $defaultSettings, // default values,
                ['prep\\Table', 3=>'string', 'string', 'string', 'string']
            ],
            func_get_args()
        );

        $this->column = Column::convert($this->param['column']);
        if (isset($this->param['table'])) {
            $this->setTable($this->param['table']);
        }

        unset($this->param['column'], $this->param['table']);
    }

    /**
     * Getter
     * @return \prep\TableInterface The table object set to this field's column
     */
    public function getTable()
    {
        $this->column->getTable();
    }

    /**
     * Setter
     * @param \prep\TableInterface|string The new table to set to this field's column
     */
    public function setTable($table)
    {
        $this->column->setTable($table);
    }

    /**
     * Returns the full SQL name and/or the SQL value of the object
     * @param int|boolean  $key       The part that you want to get (if omitted, both will be returned)
     * @param bool $acceptBool If set to true, the boolean value will be be output
     *
     * @return array|string    The property or properties
     */
    public function getProperties($key = false, $acceptBool = false)
    {
        $this->val=array();
        $return = array();

        if ($key!==1) {
            $return[] = $this->fctSQL($this->param('fctColumn'), $this->column).$this->getAlias();
        }
        if ($key!==0 && ($acceptBool || !is_bool($this->param('value')))) {
            $return[] = $this->fctSQL($this->param('fctValue'), $this->param('value'));
        }

        return $key===false ? $return : $return[0];
    }

    /**
     * Returns the alias of the Field if defined, else the name
     * @return string Alias or name of this object
     */
    public function getNickName()
    {
        return empty($this->param['alias']) ? call_user_func($this->column) : PDO::quotes($this->param['alias']);
    }

    /**
     * String representation of the object
     * @return string column=value
     */
    public function __toString()
    {
        return $this();
    }

    /**
     * Glues the properties of the Field (@see getProperties) with an operator
     * @param string|boolean|NULL $operator
     * @return string Glued properties
     */
    public function __invoke($operator = '=')
    {
        if (is_bool($operator)) {
            $operator = $operator ? '=' : '<>';
        }

        return $operator===null ?
                    $this->getProperties(0) :
                    implode($operator, $this->getProperties());
    }

    /**
     * String representation of the object to order the query
     * @return string table.field [DESC]
     */
    public function orderByStr()
    {
        return $this->getProperties(0).' '.$this->param('orderBy');
    }

    /**
     * Statically called, converts array to Field
     * @param array $array The array to convert
     * @param array $defaultSettings The default settings (optional)
     * @return FieldInterface The remaining object
     */
    protected static function convertArray(array $array, array $defaultSettings = array(null))
    {
        return new static(
            ['defaultSettings'=>$defaultSettings]+(count($array)===1 && is_array($array[0]) ? $array[0] : $array)
        );
    }

    /**
     * Getter
     * @return Column The column assigned to this field
     */
    public function getColumn()
    {
        return $this->column;
    }
}
