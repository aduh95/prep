<?php
/**
* Declare the class PDO
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   PDO.class
* @since        4.0
*/
namespace prep;

/**
* Represents a connection between PHP and a database server
*/
class PDO extends \PDO
{
    /** @var int Count the number of begun transaction to avoid too soon committing */
    private $transactionCount=0;

    /**
     * Builds a PDO object to interact with the MySQL database
     * @param string $db_name Name of the data base
     * @param string $user User name
     * @param string $password Password of the user
     * @param string $host IP or URL of the server
     * @param string $driver The driver used _(default is localhost)_
     * @param string $charset The charset used for the interactions _(default is UTF-8)_
     *
     */
    public function __construct($db_name, $user, $password, $host, $driver = 'mysql', $charset = 'UTF8')
    {
        parent::__construct(
            $driver.':charset='.$charset.';host='.$host.';dbname='.$db_name,
            $user,
            $password
        );
    }

    /**
     * Begins a SQL transaction if this object is not already inside one
     * @return bool
     */
    public function beginTransaction()
    {
        return !$this->transactionCount++ && parent::beginTransaction();
    }

    /**
     * Commits a transaction if all transactions have been committed
     * @return bool
     */
    public function commit()
    {
        return (!--$this->transactionCount) && parent::commit();
    }

    /**
     * Rolls back all the transactions begun
     * @return bool
     */
    public function rollBack()
    {
        $this->transactionCount = 0;
        return parent::rollBack();
    }

    /**
     * Quotes a string for use in a query
     * @param string $str The string to be quoted
     * @return string|false Returns a quoted string that is theoretically safe to pass into an SQL statement.
     *                      Returns FALSE if the driver does not support quoting in this way.
     */
    public static function quotes($str)
    {
        return '`'.str_replace('`', '``', $str).'`';
    }
}
