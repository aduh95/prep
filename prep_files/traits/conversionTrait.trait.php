<?php
/**
* Declare the  convertibleTrait trait
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   conversionTrait.trait
* @since        4.2
*
*/
namespace prep;

/**
 * Contains method to convert object from class to another.
 */
trait conversionTrait
{
    /**
     * Statically called, converts anything to an object of the class that calls it, if possible
     * @param mixed $obj The object to convert
     * @return mixed A converted object
     * @throws \prep\ConvertException
     */
    public static function convert($obj)
    {
        // Check if it has been retrieved from a func_get_args() call
        if (is_array($obj) && count($obj)===1 && isset($obj[0])) {
            return self::convert($obj[0]);
        }

        try {
            if (is_object($obj)) {
                if (is_a(
                    $obj,
                    array_reduce(class_implements(__CLASS__), function ($pv, $cv) {
                        return substr($cv, 0, strlen(__CLASS__))===__CLASS__ ? $cv : $pv;
                    }, __CLASS__)
                )) {
                    return $obj;
                } elseif (is_callable($conversion_method = [$obj, 'get'.substr(strstr(__CLASS__, '\\'), 1)])) {
                    return self::convert(call_user_func($conversion_method));
                }
            } elseif (empty($obj)) {
                return self::convert(new EmptyTable);
            } elseif (is_callable($convert_method = 'self::convert'.gettype($obj))) {
                return call_user_func_array($convert_method, func_get_args());
            } else {
                return new static($obj);
            }
        } catch (Exception $e) {
            throw new ConvertException(__CLASS__, $obj, $e);
        }
        throw new ConvertException(__CLASS__, $obj);
    }
}
