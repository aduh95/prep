<?php
/**
* Declare the  prepMethodsTrait trait, containing methods used by most of the PREP's classes.
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   prepMethodsTrait.trait
* @since        3.7
*
*/
namespace prep;

/**
 * Some methods used in the PREP's classes that implements prep\PrepMethodsInterface.
 */
trait prepMethodsTrait
{
    /**
     * @var array $val      Non SQL safe value
     */
    protected $val = array();

    /**
     * @var PrepMethodsInterface[] $elements Contained elements of this object
     */
    protected $elements = array();

    /**
     * @var array $param    The parameters for this object
     */
    protected $param = array();

    /**
     * Returns or sets a parameter of the current object
     *
     * Return a parameter: `mixed param(string $parameter);`
     * Set a parameter: `mixed param(string $parameter, mixed $value);`
     * Set several parameters: `bool param(array $parameters);`
     * @param string $param
     * @return mixed
     *
     * Sets a parameter of the current object
     * @param string $param
     * @param mixed $value
     * @return mixed
     *
     * Sets several parameters of the current object
     * @param array $params
     * @return bool
     */
    public function param($param = false /*, $value*/)
    {
        if (!$param) {
            return $this->param;
        }
        if (is_array($param)) {
            return array_walk($param, function ($value, $param, $that) {
                $that->param($param, $value);
            }, $this);
        }
        if (func_num_args()>1) {
            $this->param[$param] = func_get_arg(1);
        }
        if (!array_key_exists($param, $this->param)) {
            return false;
        }

        return $this->param[$param];
    }

    /**
     * Returns the protected values of the objects within itself (if there is any)
     * @return array
     */
    protected function getRecursiveVal()
    {
        return self::isEmpty() ?
            array() :
            call_user_func_array(
                'array_merge',
                array_map(function ($elem) {
                    return $elem->getVal();
                }, $this->elements)
            );
    }

    /**
     * Returns the protected values of itself or the objects within itself
     * @return mixed
     */
    public function getVal()
    {
        return self::isEmpty() ?
            $this->val :
            $this->getRecursiveVal();
    }

    /**
     * Tells if this object is empty
     * @return boolean
     */
    public function isEmpty()
    {
        return empty($this->elements);
    }

    /**
     * Implode an array to a string with a callback
     * @param array $array The array of values to implode
     * @param callable $callback The callback use to transform the values
     * @param string $glue
     *
     * @return string
     */
    protected static function implode($array, $callback = 'strval', $glue = ',')
    {
        return implode($glue, array_map($callback, $array));
    }
}
