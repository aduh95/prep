<?php
/**
* Declare the databaseMethodsTrait trait, containing methods useful for building parts of a SQL query.
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   databaseMethodsTrait.trait
* @since        3.0.2
*
*/
namespace prep;

use DateTimeInterface;

/**
 * Some useful methods to build a SQL query in the prep classes.
 */
trait databaseMethodsTrait
{
    /**
     * Generates SQL code to call a SQL function protecting arguments
     *
     * ->fctSQL($func_name[, $arg1[, $arg2[, ...]]])
     *
     * @param string $func_name: Name of the function. If empty, only first argument will be passed
     * @param mixed $args: if it is an array, each element is passed as an argument to $func_name,
     *                  or ignored (but the first) if $func_name is empty
     *
     * @return string
     */
    protected function fctSQL($func_name, $args)
    {
        if (!is_array($args)) {
            $args = array($args);
        }

        return empty($func_name) ?
            $this->safeVal(array_shift($args)) :
            $func_name.'('.self::implode($args, [$this,'safeVal']).')';
    }

    /**
     * Protects non numeric value from SQL injections
     *
     * @param mixed $value The user input you want to send to database
     *
     * @return mixed
     */
    protected function safeVal($value)
    {
        if (is_object($value)) {
            if (is_callable($value)) {
                if ($value instanceof DirectSQL && !$value->isEmpty()) {
                    array_push($this->val, ...$value->getVal());
                }

                return  $value();
            } elseif ($value instanceof DateTimeInterface) {
                return "'".$value->format(SQLQuery::MYSQL_DATE_FORMAT)."'";
            }
        } elseif (is_numeric($value)) {
            return $value;
        } elseif ($value===null) {
            return 'NULL';
        } elseif (is_bool($value)) {
            return $value ? 'TRUE' : 'FALSE';
        } else {
            $this->val[] = strval($value);
            return '?';
        }
    }

    /**
     * If an alias is set, returns the SQL fragment to define it
     * @return string
     */
    protected function getAlias()
    {
        return empty($this->param['alias']) ? null : ' AS `'.$this->param['alias'].'`';
    }
}
