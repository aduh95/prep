<?php
/**
* Declare the class Where, which uses trait  databaseMethodsTrait
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Where.class
* @since        2.2
*
*/
namespace prep;

/**
 * Build where conditions for prep\Select and prep\Query.
 */
class Where implements WhereInterface
{
    use   prepMethodsTrait, conversionTrait;

    /**
     * @var array Contains the default values for the Condition constructor
     */
    public $defaultSettings = array();

    /**
     * Object constructor
     *
     * Method definition:
     *
     * - Where::__construct(ConditionInterface $condition1, ...)
     * - Where::__construct(ConditionInterface[] $conditions)
     *
     * @param ConditionInterface|ConditionInterface[] $condition1
     * @param ConditionInterface ..
     */
    public function __construct()
    {
        if (func_num_args()) {
            array_map(
                'self::addCondition',
                is_array($arg = func_get_arg(0)) && !empty($arg[0]) && is_array($arg[0]) ?
                    $arg :
                    func_get_args()
            );
        }
    }

    /**
     * Add a selection criterion
     *
     * @param ~Condition $condition The condition to add (same argument(s) as Condition::__construct())
     * @return ConditionInterface[] All the conditions of this object
     */
    public function addCondition($condition)
    {
        return $this->elements[] = Condition::convert(func_get_args(), $this->defaultSettings);
    }

    /**
     * Choose a default table for the Condition objects.
     * Set the Table of each Condition already set in $elements that have no Table yet to the one passed on argument
     *
     * __Tip:__ to set the Table for the next conditions add to the object,
     *          you may set $object->defaultSettings['table'].
     *
     * @param ~Table $table The default table to set
     *
     * @return void
     */
    public function setDefaultTable($table)
    {
        $table = Table::convert($table);
        array_map(function ($cond) use ($table) {
            $condTable = $cond->getTable();

            if (!is_object($condTable) || !($condTable instanceof EmptyTable)) {
                $cond->setTable($table);
            }
        }, $this->elements);
    }

    /**
     * Converts array to Where, if possible
     * @param array $array The array to convert
     * @return Where The created Where object
     */
    protected static function convertArray(array $array)
    {
        $return = [];
        foreach ($array as $key => $where) {
            if (is_numeric($key)) {
                $return[]=$where;
            } else {
                $return[]=new Condition(null, $key, $where);
            }
        }
        return new self($return);
    }

    /**
     * Build a SQL set of conditions
     * @return string The SQL where statement
     */
    public function __toString()
    {
        if ($this->isEmpty()) {
            return '1';
        }
        $elements = $this->elements;
        return array_shift($elements)->getSQLValue().self::implode($elements, 'strval', ' ');
    }
}
