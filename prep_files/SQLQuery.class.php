<?php
/**
* Here is defined an abstract class, you won't deal with it directly unless you want change the behavior of some methods
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   SQLQuery.class
* @since        2.0.2
*
*/
namespace prep;

/**
 * Shared methods&proprieties of PREP classes Query, CustomQuery, Select.
 */
abstract class SQLQuery
{
    const MYSQL_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var \prep\Where $where The where statement for the query
     */
    public $where;

    /**
     * @var \PDO                    $pdo    The PDO object to execute the query
     */
    protected $pdo;

    /**
     * @var \prep\TableInterface[]   $tables The table(s) used by the query
     */
    protected $tables;

    /**
     * @var string[]                $values The non SQL safe values
     */
    protected $values = array();

    /**
     * @var string                  $limit  The limit set to the query on the SQL format
     */
    protected $limit;

    use  prepMethodsTrait {
        getVal as protected;
    }

    /**
     * Constructor
     * @param \PDO $pdo The PDO object on which the query will be prepared and executed
     * @param ~Table $table The main table of the query
     */
    public function __construct(\PDO $pdo, $table)
    {
        $this->pdo = $pdo;
        $this->tables = array(Table::convert($table));
        $this->where = new Where;
    }

    /**
     * Set the limit.
     * It follows this definition: setLimit(array([$offset,] $limit))
     *
     * @internal param int $offset : default is 0
     * @internal param int $limit
     *
     */
    public function setLimit()
    {
        $lim = ArgumentException::checkArgs(
            [['offset', 'limit'], 3=>['integer', 'integer']],
            func_get_args()
        );

        if (!empty($lim)) {
            $this->limit = implode(',', $lim);
        }
    }

    /**
     * Getter
     * @return string The SQL query generated
     */
    abstract public function getQuery();

    /**
     * Alias for getQuery()
     * @return string The SQL query of this object
     */
    public function __toString()
    {
        return $this->getQuery();
    }

    /**
     * To recall the "dangerous" values you want to make safe
     * @return array
     */
    protected function getVal()
    {
        return array_merge(
            array_merge(
                $this->getRecursiveVal(),
                ...array_map(function ($table) {
                    return $table->getVal();
                }, $this->tables)
            ),
            $this->where->getVal()
        );
    }

    /**
     * Prepare and execute the query
     * @return \PDOStatement
     */
    public function exec()
    {
        $pdo = $this->pdo->prepare($this->getQuery());
        $pdo->execute($this->getVal());
        return $pdo;
    }

    /**
     * Outputs the debug information from MySQL and PHP. No change to the database will be executed.
     * @return array
     */
    public function __debugInfo()
    {
        $this->pdo->beginTransaction();
        $PDOStatement = $this->exec();

        $return = [
                'PDOStatement::rowCount()'=>$PDOStatement->rowCount(),
                'SQL_query'=>$this->getQuery(),
                'parameters'=>$this->getVal(),
                'MySQL_errorInfo'=>$PDOStatement->errorInfo()
            ];

        $this->pdo->rollBack();
        return $return;
    }
}
