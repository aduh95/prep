<?php
/**
* Declare the prepMethodsTrait interface
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   prepMethodsTrait.interface
* @since        4.2
*
*/
namespace prep;

/**
 * Lists all the public methods that should have the classes using the prepMethodsTrait
 *
 * @since 4.2.0
 */
interface PrepMethodsInterface
{
    /**
     * Returns or sets a parameter of the current object
     * @param STRING|bool $param The name of the parameter to retrieve
     * @return mixed
     */
    public function param($param = false);

    /**
     * Returns the protected values of itself or the objects within itself
     */
    public function getVal();

    /**
     * Tells if this object is empty
     * @return boolean
     */
    public function isEmpty();
}
