<?php
/**
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   WhereInterface.interface
* @since        4.3.0
*
*/
namespace prep;

/**
 * Represents a Where object.
 *
 * @since 4.3.0
 */
interface WhereInterface extends PrepMethodsInterface
{
    /**
     * Add a selection criterion
     *
     * @param ~Condition $condition The condition to add (same argument(s) as Condition::__construct())
     * @return ConditionInterface[] All the conditions of this object
     */
    public function addCondition($condition);

    /**
     * Choose a default table for the Condition objects
     *
     * @param Table $table The default table to set
     */
    public function setDefaultTable($table);

    /**
     * Build a SQL set of conditions
     * @return string The SQL where statement
     */
    public function __toString();
}
