<?php
/**
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   FieldInterface.interface
* @since        4.2
*
*/
namespace prep;

/**
 * Represents a field for a query
 */
interface FieldInterface extends PrepMethodsInterface
{
    public function getProperties();

    public function getNickName();

    public function orderByStr();

    public function __toString();

    public function __invoke();
}
