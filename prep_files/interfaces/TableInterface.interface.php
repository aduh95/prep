<?php
/**
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   TableInterface.interface
* @since        4.2
*
*/
namespace prep;

/**
 * Represents a SQL table
 */
interface TableInterface extends PrepMethodsInterface
{
    public function getNickName();

    public function __toString();
}
