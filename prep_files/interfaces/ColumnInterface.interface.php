<?php
/**
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   ColumnInterface.interface
* @since        4.2
*
*/
namespace prep;

/**
 * Represents a SQL column
 */
interface ColumnInterface extends PrepMethodsInterface
{
    public function setTable($table);

    public function __toString();

    public function __invoke();
}
