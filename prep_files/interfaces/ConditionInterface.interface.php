<?php
/**
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   ConditionInterface.interface
* @since        4.2
*
*/
namespace prep;

/**
 * Represents a condition for prep\Where
 */
interface ConditionInterface extends FieldInterface
{
    public function getSQLValue();
}
