<?php
/**
* Declare the class Select, that builds selection SQL queries
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Select.class
* @since        1.2
*
*/
namespace prep;

/**
 * Prepare a query of selection from PDO
 */
class Select extends SQLQuery
{
    /**
     * @var bool $count  Describes if the query is counting
     */
    public $count=false;

    /**
     * @var bool $rollup Describes if the query is using ROLL UP
     */
    public $rollup=false;

    /**
     * @var \prep\FieldInterface[] $order_by Contains the field that will order the selection
     */
    private $orderBy = array();
    
    /**
     * @var \prep\FieldInterface[] $group_by Contains the field that will group the selection
     */
    private $groupBy=array();


    /**
     * Description in prep\SQLQuery
     * @param ~Column $column
     * @param ~Table $table : Default value for $table is the main table
     * @return mixed|Column
     */
    public function addField($column, $table = null)
    {
        return $this->elements[] = is_object($column) ?
            Column::convert($column) :
            new Column(
                $column,
                empty($table) || $table instanceof EmptyTable ? $this->tables[0] : Table::convert($table)
            );
    }

    /**
     * Add one or more table to join
     * @param array $tables
     * @internal param $ ~Table $table1
     * @internal param $ ...
     *
     */
    public function addJoin(...$tables)
    {
        foreach ($tables as $table) {
            $this->tables[]=Table::convert($table);
        }
    }

    /**
     * Search the table by alias or name. If no argument passed, main table is returned.
     * @param string $search
     *
     * @return TableInterface|boolean The founded table or false in case of failure
     */
    public function getTable($search = '')
    {
        if (!$search || count($this->tables)<2) {
            return $this->tables[0];
        }
        foreach ($this->tables as $table) {
            if ($table->param('alias') === $search) {
                return $table;
            }
        }
        foreach ($this->tables as $table) {
            if ($table->param('name') === $search) {
                return $table;
            }
        }

        return false;
    }

    /**
     * Count the number of result of the selection, regardless of limit
     * @param 0:$groupBy    => array
     * @param 1:$rollup      => bool
     *
     * @return int or array
     */
    public function count()
    {
        $args = ArgumentException::checkArgs([['groupBy', 'rollup'], [], [false, false]], func_get_args());
        // extract($args);

        $this->count=true;
        if ($args['groupBy'] && $args['rollup']) {
            $this->groupBy = array();
            $this->rollup=$args['rollup'];
            array_map('self::groupBy', (array)$args['groupBy']);
            $return=$this->exec()->fetchAll();
            $this->groupBy = array();
        } else {
            $return = intval($this->exec()->fetchColumn());
        }
        $this->count=false;
        return $return;
    }

    /**
     * Set the fields that will be used to order the results.
     * You may specify in the param of the field the key 'order' to ASC or DESC.
     * @param ~Field $champ: If a string is passed, a Field object is generate.
     *
     * @return Field|null
     */
    public function orderBy($champ)
    {
        if (!empty($champ)) {
            return $this->orderBy[]= Field::convert($champ);
        }

        return null;
    }

    /**
     * Set the fields that will be used to group the results.
     * @param ~Field $champ: If a string is passed, a Field object is generate.
     *
     * @return Field
     */
    public function groupBy($champ)
    {
        if (!empty($champ)) {
            $this->groupBy[]= Column::convert($champ);
        }
    }

    /**
     * Getter
     * @return string The SQL query generated
     */
    public function getQuery()
    {
        if ($this->count && !$this->groupBy) {
            $query='SELECT COUNT(*)';
        } elseif ($this->isEmpty()) {
            $query='SELECT *';
        } else {
            $champs = '';
            foreach ($this->elements as $champ) {
                $champs.= ','.$champ(null);
            }
            $query = 'SELECT '.substr($champs.($this->count?',COUNT(*)':null), 1);
        }

        $query.= ' FROM '.substr(
            self::implode($this->tables, 'strval', "\n"),
            5*!is_a($this->tables[0], 'prep\DirectSQL')
        );
        if (!$this->where->isEmpty()) {
            $query.= ' WHERE '.$this->where;
        }
        if (!empty($this->groupBy)) {
            $query.= ' GROUP BY '.self::implode($this->groupBy).($this->rollup ? ' WITH ROLLUP' : null);
        }
        if (!empty($this->orderBy)) {
            $query.= ' ORDER BY '.self::implode($this->orderBy, function (FieldInterface $f) {
                return $f->orderByStr();
            });
        }
        if (!empty($this->limit) && !$this->count) {
            $query.= ' LIMIT '.$this->limit;
        }

        return $query;
    }

    /**
     * To recall the "dangerous" values you want to make safe
     * @return array
     */
    protected function getVal()
    {
        return array_merge(
            parent::getVal(),
            ...array_map(function ($field) {
                return $field->getVal();
            }, array_merge($this->groupBy, $this->orderBy))
        );
    }
}
