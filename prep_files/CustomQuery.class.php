<?php
/**
* Declare the class Query, that extends SQLQuery, which uses trait  databaseMethodsTrait
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   CustomQuery.class
* @since        3.8
*
*/
namespace prep;

/**
 * Build a query with a custom SQL statement using PDO
 */
class CustomQuery extends SQLQuery
{
    /**
     * @var string  $query Contains the custom query
     */
    protected $query;
    
    /**
     * @var array   $param The non safe values that will be added to the prepared query
     */
    protected $param;


    /**
     * Class constructor
     * @param PDO $PDO      PDO object used to prepare the query
     * @param string $query SQL statement
     * @param array $param  Optional, if you want to protect values with protected query
     */
    public function __construct($PDO, $query, array $param = array())
    {
        parent::__construct($PDO, null);
        $this->query = $query;
        $this->param = $param;
    }

    /**
     * Getter
     * @return string The SQL query provided in the constructor
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Getter
     * @return array The protected values provided in the constructor
     */
    protected function getVal()
    {
        return $this->param;
    }
}
