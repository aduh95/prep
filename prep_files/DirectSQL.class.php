<?php
/**
* Declare the class DirectSQL
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   DirectSQL.class
* @since        4.0.0
*
*/
namespace prep;

/**
* DirectSQL allows you to put direct SQL code in your query, to make them faster or easier to write
*/
class DirectSQL implements ColumnInterface, FieldInterface, ConditionInterface, TableInterface
{
    /**
     * @var string $sql The SQL string to inject
     */
    protected $sql;

    /**
     * @var array  $val The non SQL safe values
     */
    protected $val;

    /**
     * Constructor
     * @param string    $sql               The SQL string to inject
     * @param string[] ...$protected_values  The non SQL safe values
     */
    public function __construct($sql, ...$protected_values)
    {
        $this->sql = strval($sql);
        $this->val = $protected_values;
    }

    /**
     * Defining the methods to make the object act like the object from the other classes
     * @return string The SQL string to inject
     */
    public function __toString()
    {
        return $this->sql;
    }

    /**
     * Alias for __toString
     * @return string
     */
    public function __invoke()
    {
        return strval($this);
    }

    // ========= PrepMethodsInterface =========

    /**
     * Only for compatibility
     * @param bool|string $param false
     * @return string|null
     */
    public function param($param = false)
    {
        if ($param==='alias') {
            return strval($this);
        }

        return null;
    }

    /**
     * Returns the protected values of the current object
     * @return array
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * Returns if there is any protected value for the current object
     * @return bool
     */
    public function isEmpty()
    {
        return empty($this->val);
    }


    // ========= ColumnInterface =========

    /**
     * Only for compatibility
     * @param null
     */
    public function setTable($table)
    {
    }

    // ========= FieldInterface =========

    /**
     * Only for compatibility
     */
    public function getProperties()
    {
    }

    /**
     * Alias for __toString
     * @return string
     */
    public function orderByStr()
    {
        return strval($this);
    }


    // ========= ConditionInterface =========

    /**
     * Alias for __toString
     * @return string
     */
    public function getSQLValue()
    {
        return strval($this);
    }

    // ========= TableInterface =========

    /**
     * Alias for __toString
     * @return string
     */
    public function getNickName()
    {
        return strval($this);
    }
}
