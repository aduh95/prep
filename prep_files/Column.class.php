<?php
/**
* Declare the class Column
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Column.class
* @since        3.0.5
*
*/
namespace prep;

/**
 * Set a field name.
 * Is not protected by  databaseMethodsTrait::safeVal, so it is interpreted by MySQL as a field name
 *
 * @since 3.0.5
 */
class Column implements ColumnInterface
{
    use conversionTrait;

    /**
     * @var \prep\TableInterface $table  The table of this column
     */
    protected $table;

    /**
     * @var string $column The name of the column
     */
    protected $column;


    /**
     * Constructor
     *
     * This method follows those definitions:
     *
     * - Column::__construct(~string $columnName[, ~Table $table])
     * - Column::__construct(array $arguments)
     *
     * Parameters:
     *
     * 0. $name  => ~string (if empty, "*" will be sent)
     * 1. $table => ~Table
     */
    public function __construct()
    {
        list($this->column, $this->table) = array_values(ArgumentException::checkArgs(
            [
                ['name', 'table'],
                ['name'], // non optional arg
                ['table'=>null], //default value
                ['string', 'prep\Table'] // type
            ],
            func_get_args()
        ));
    }

    /**
     * Getter
     * @return \prep\TableInterface
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Setter
     * @param \prep\TableInterface $table The table to set to this object
     *
     * @return void
     */
    public function setTable($table)
    {
        $this->table = Table::convert($table);
    }

    /**
     * Getter
     * @return string The table name or alias, then a dot
     */
    private function getTableNickName()
    {
        $nickname = $this->table->getNickName().'.';
        return strlen($nickname)>3 ? $nickname : '';
    }

    /**
     * String representation of the object
     * @return string The complete name for a SQL query
     */
    public function __toString()
    {
        return $this();
    }

    /**
     * Alias of __toString
     * @return string The complete name for a SQL query
     */
    public function __invoke()
    {
        return $this->getTableNickName().($this->column ? '`'.str_replace('`', '``', $this->column).'`' : '*');
    }


    // ========= Conversion methods =========

    /**
     * Convert method from Column to Field
     * @return Field A Field equivalent to this object
     */
    public function getField()
    {
        return new Field(['column'=>$this, 'value'=>true]);
    }

    /**
     * Convert method from Column to Condition
     * @return Condition A Condition equivalent to this object
     */
    public function getCondition()
    {
        return new Condition(['column'=>$this->column]);
    }


    // ========= PrepMethodsInterface =========

    /**
     * Only for compatibility, non used
     * @return array Empty array
     */
    public function getVal()
    {
        return [];
    }

    /**
     * Only for compatibility, non used
     * @param string
     * @return mixed|void
     */
    public function param($value = '')
    {
    }

    /**
     * Only for compatibility, non used
     * @return bool true
     */
    public function isEmpty()
    {
        return true;
    }
}
