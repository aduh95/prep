<?php
/**
* Declare the class Table, which uses trait  databaseMethodsTrait
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Table.class
* @since        3.0.4
*
*/
namespace prep;

/**
 * Describe a table that will be used in a MySQL query.
 */
class Table implements TableInterface
{
    use   prepMethodsTrait,  databaseMethodsTrait, conversionTrait;

    /**
     * Create a Table object.
     *
     * This method follows those definitions:
     *
     * - Table::__construct(string $name[, string $alias[, mixed $join[, ~Where $ON]]]);
     * - Table::__construct(array $array);
     *
     * Parameters:
     *
     * 0. name=>   string  Name of the table
     * 1. alias=>  string  Alias of the table
     * 2. join=>   mixed   Type of the join ('INNER', 'LEFT OUTER', 'NATURAL', ...)
     * 3. ON=>     ~Where  The table will join when the condition is true
     *
     */
    public function __construct()
    {
        $this->param = ArgumentException::checkArgs(
            [
                ['name', 'alias', 'join', 'ON'],
                ['name'],
                [],
                ['string', 'string', 'ON'=>'prep\\Where']
            ],
            func_get_args()
        );

        if (!empty($this->param['ON'])) {
            $this->param['ON']->setDefaultTable($this);
        }
    }

    /**
     * Returns the alias of the Table if defined, else the name
     * @return string
     */
    public function getNickName()
    {
        return PDO::quotes(empty($this->param['alias']) ? $this->param['name'] : $this->param['alias']);
    }

    /**
     * Build a SQL fragment to use the Table in your query
     * @return string
     */
    public function __toString()
    {
        return (empty($this->param['join']) ? null : $this->param['join']).' JOIN '.
            PDO::quotes($this->param['name']).$this->getAlias().
            (empty($this->param['ON']) ? null : ' ON '.$this->param['ON']);
    }
}
