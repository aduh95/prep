<?php
/**
* Declare the class Query, that extends SQLQuery, which uses trait  databaseMethodsTrait
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Query.class
* @since        1.0
*
*/
namespace prep;

/**
 * Build a query such as INSERT INTO, UPDATE or DELETE FROM from PDO
 */
class Query extends SQLQuery
{
    /**
     * @var int $type The type of the query intended
     */
    private $type; //int

    const QUERIES = array('INSERT INTO', 'UPDATE', 'DELETE FROM'),
            INSERT = 0,
            UPDATE = 1,
            DELETE = 2;

    /**
     * Class constructor
     * @param PDO $bdd: PDO object used to prepare the query
     * @param int $type: Using a constant declared above; if the type is invalid, INSERT INTO is selected by default
     * @param ~Table $table: the query will be executed on this table
     */
    public function __construct($bdd, $type, $table)
    {
        $this->type = array_key_exists($type, self::QUERIES) ? $type : self::INSERT;
        parent::__construct($bdd, $table);
    }

    /**
     * Add a field of insertion or updating to the Query object
     *
     * Method definitions:
     *
     * - Query::addField(Field $field);
     * - Query::addField(ColumnInterface $column, mixed $value);
     *
     * @param $field
     * @return Field The Field object added
     */
    public function addField($field)
    {
        return $this->elements[] = is_object($field) && $field instanceof Field ?
            $field :
            Field::convert(ArgumentException::checkArgs([['column', 'value'], ['column']], func_get_args()));
    }

    /**
     * Getter
     * @return string The SQL query generated
     */
    public function getQuery()
    {
        $query = self::QUERIES[$this->type] .' '.substr($this->tables[0], 5);
        switch ($this->type) {
            case self::INSERT:
                $champs = array_map(
                    function ($champ) {
                        return $champ->getProperties(false, true);
                    },
                    $this->elements
                );
                $query.=
                    ' ('.
                    self::implode($champs, 'array_shift').
                    ') VALUES ('.
                    self::implode($champs, 'array_pop').
                    ')';
                break;

            case self::UPDATE:
                $query.=' SET '. self::implode($this->elements, function ($element) {
                    return implode('=', $element->getProperties(false, true)); // forcing boolean values to output
                });
                break;
            // Nothing to do with DELETE
        }
        if ($this->type != self::INSERT && !$this->where->isEmpty()) {
            $query.= ' WHERE '.$this->where;
        }
        if (!empty($this->limit)) {
            $query.= ' LIMIT '.$this->limit;
        }

        return $query;
    }
}
