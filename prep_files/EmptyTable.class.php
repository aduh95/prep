<?php
/**
* Declare the class EmptyTable, which extends prep\Table
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Table.class
* @since        4.2
*
*/
namespace prep;

/**
 * Describe a table that will be used in a MySQL query with no name.
 */
class EmptyTable extends Table
{
    /**
     * Create a EmptyTable object
     */
    public function __construct()
    {
        $this->param = ['name'=>null];
    }

    /**
     * Build a SQL fragment to use the Table in your query
     * @return string Empty string
     */
    public function __toString()
    {
        return '';
    }
}
