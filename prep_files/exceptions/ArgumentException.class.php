<?php
/**
* Declare the class ArgumentException
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   ArgumentException.class
* @since        4.1
*/
namespace prep;

/**
 * Represents an error triggers by a misuse of the arguments when calling a PREP's method
 *
 * @since 4.3.0 The class is no longer abstract
 * @since 4.2.0 The class is defined abstract
 * @since 4.1
 */
class ArgumentException extends Exception
{
    const   DEFAULT_VAL_KEY = 1,
            EXPECTED_TYPE_KEY = 0;

    /**
     * Filter the arguments provided
     * @param array $keys
         * @key 0=> list of all the acceptable arguments
         * @key 1=> list of required arguments (default value will be ignored for each required argument)
         * @key 2=> list of the default value for each argument
         * @key 3=> list of the expected types for each argument
     * @param array $args: The argument passed to the function. May be a list of argument, or an array into the array
     *
     * @return array
     * @throws \prep\ArgumentException           If a key passed in $keys[1] is not in $keys[0]
     * @throws \prep\MissingArgumentException    If a key passed in $keys[1] is not in $args
     * @throws \prep\WrongTypeArgumentException  If the argument passed in $args does not match
     *                                          the type requested in $keys[3]
     */
    public static function checkArgs(array $keys, array $args)
    {
        $return = array();
        $keys = array_reverse(array_replace(array_fill(0, 4, array()), $keys));
        $args = array_change_key_case((count($args)===1 && is_array($args[0])) ? $args[0] : $args);

        // Getting the keys provided to be more efficient
        $args_keys = array();
        foreach ($args as $key => $value) {
            if(is_string($key)) {
                $key = str_replace('_', '', $key);
                $args[$key] = $value;
            }
            $args_keys[] = $key;
        }
        $args_keys = array_flip($args_keys);

        // Check if all the required indexes are in the list of possible arguments
        $list_args = array_pop($keys);
        $required_indexes = array_map(function ($arg) use ($list_args) {
            $return= array_search($arg, $list_args);

            if ($return===false) {
                throw new self('Cannot require argument "'.$arg.'" that is not in the list of arguments');
            }

            return $return;
        }, array_pop($keys));

        // Get only the accepted arguments
        foreach ($list_args as $index => $key) {
            if (isset($args_keys[$tmp = $index]) || isset($args_keys[$tmp = strtolower($key)])) {
                // If the parameter has been given
                $return[$key]= $args[$tmp];
            } elseif (in_array($index, $required_indexes)) {
                // if the parameter is not optional
                throw new MissingArgumentException($index, $key);
            } elseif (
                array_key_exists($tmp = $index, $keys[self::DEFAULT_VAL_KEY]) ||
                array_key_exists($tmp = $key, $keys[self::DEFAULT_VAL_KEY])
            ) {
                $return[$key] = $keys[self::DEFAULT_VAL_KEY][$tmp]; // Default value
            } else {
                continue;
            }

            // Check the type of the parameter
            if (
                isset($keys[self::EXPECTED_TYPE_KEY][$tmp = $index]) ||
                isset($keys[self::EXPECTED_TYPE_KEY][$tmp = strtolower($key)])
            ) {
                $return[$key] = self::convert($return[$key], $keys[self::EXPECTED_TYPE_KEY][$tmp], $index, $key);
            }
        }
        return $return;
    }

    /**
     * If possible, converts a given object/variable into the type requested
     * @param mixed $obj The object you want to convert
     * @param string $type The type / class name / interface name to convert the $obj
     * @param int $index The index of the parameter given
     * @param string $key The name of the parameter
     *
     * @return mixed
     * @throws WrongTypeArgumentException, ConvertException
     */
    private static function convert($obj, $type, $index, $key)
    {
        if (gettype($obj)===$type || is_a($obj, $type)) {
            return $obj;
        } elseif (is_callable($type.'::convert')) {
            try {
                return call_user_func($type.'::convert', $obj);
            } catch (ConvertException $e) {
                throw new WrongTypeArgumentException($index, $key, $type, Exception::getType($obj), $e);
            }
        } else {
            switch ($obj===null || is_array($obj) ? null : $type) {
                case 'string':
                case 'str':
                    return strval($obj);

                case 'integer':
                case 'int':
                    return intval($obj);

                case 'number':
                case 'float':
                    return floatval($obj);

                case 'boolean':
                case 'bool':
                    return boolval($obj);

                default:
                    throw new WrongTypeArgumentException($index, $key, $type, Exception::getType($obj));
                    break;
            }
        }
    }
}
