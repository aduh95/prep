<?php
/**
* Declare the class Exception
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Exception.class
* @since        3.8
*/
namespace prep;

/**
 * Base class for all Exceptions in the the PREP package
 *
 * @since 3.8
 */
class Exception extends \Exception
{
    /**
     * Constructor of the prep\Exception
     * @param string $message An error message
     * @param \Exception $previousException The previous Exception (optional)
     * @uses \Prep::getCallingMethodName
     */
    public function __construct($message = 'Unknown method', \Exception $previousException = null)
    {
        $trace = $this->getTrace();
        $i=0;

        while (isset($trace[$i]['class']) && !strncasecmp($trace[$i]['class'], 'prep', 4)) {
            $i++;
        }

        if (isset(
            $trace[--$i],
            $trace[$i]['line'],
            $trace[$i]['file'],
            $trace[$i]['class'],
            $trace[$i]['function'],
            $trace[$i]['type']
        )) {
            $this->line=$trace[$i]['line'];
            $this->file=$trace[$i]['file'];

            $method = $trace[$i]['function']==='__callStatic' ?
                \Prep::getCallingMethodName() :
                $trace[$i]['class'].$trace[$i]['type'].$trace[$i]['function'];
        } else {
            $method= null;
        }

        parent::__construct(
            'Prep error: '.$message."! \n".($method===null ? null:'Exception raised when calling '.$method),
            E_RECOVERABLE_ERROR,
            $previousException
        );
    }

    /**
     * Returns the name of the class of the object, or the type if it's not an object
     * @param mixed $var The variable you want to know the type
     *
     * @return string The class name of the object or type of the variable
     */
    protected static function getType($var)
    {
        return call_user_func('get'.(is_object($var) ? '_class' : 'type'), $var);
    }
}
