<?php
/**
* Declare the class MySQLErrorException
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   MySQLErrorException.class
* @since        4.1
*/
namespace prep;

/**
 * Thrown when MySQL triggers an error using PREP
 *
 * @since 4.1
 */
class MySQLErrorException extends Exception
{
    /**
     * @var \PDOStatement $errorStatement The PDOStatement object that contains the query triggering the error
     */
    private $errorStatement;

    /**
     * Constructor of the prep\MySQLErrorException
     * @param \PDOStatement $PDOStatement       The PDOStatement object that contains the query
     * @param \Exception    $previousException  The previous Exception (optional)
     */
    public function __construct(\PDOStatement $PDOStatement = null, $previousException = null)
    {
        $this->errorStatement = $PDOStatement;
        parent::__construct('MySQL returns an error ('.$this->getMySQLErrorInfo()[2].')');
    }

    /**
     * Returns the error state code from MySQL
     * @see PDO::errorCode
     * @return string|NULL
     */
    public function getMySQLError()
    {
        return $this->errorStatement->errorCode();
    }

    /**
     * Returns the error code from MySQL
     * @see PDO::errorCode
     * @return string|NULL
     */
    public function getMySQLErrorCode()
    {
        return $this->getMySQLErrorInfo()[1];
    }

    /**
     * Returns the error info from MySQL
     * @see PDO::errorCode
     * @return string|NULL
     */
    public function getMySQLErrorInfo()
    {
        return $this->errorStatement->errorInfo();
    }

    /**
     * Check if the SQLSTATE corresponds to a MySQL error code
     * @param \PDOStatement $PDOStatement The PDOStatement that executes the query
     *
     * @return void
     * @throws MySQLErrorException If a MySQL error triggers
     */
    public static function checkError(\PDOStatement $PDOStatement)
    {
        $error = $PDOStatement->errorCode();
        if (!empty($error) && $error!=='00000') {
            throw new MySQLErrorException($PDOStatement);
        }
    }
}
