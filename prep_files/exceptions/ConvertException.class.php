<?php
/**
* Declare the class ConvertException
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   ConvertException.class
* @since        4.1
*/
namespace prep;

/**
 * Represents a conversion error, thrown when a PREP conversion fails
 */
class ConvertException extends Exception
{
    /**
     * @var string $class The name of the class destination
     */
    private $class;
    
    /**
     * @var mixed  $obj   The object that cannot be converted to $class
     */
    private $obj;


    /**
     * Constructor of the prep\ConvertException
     * @param string     $class             The class that raises the Exception
     * @param mixed      $obj               The object you're trying to convert to $class
     * @param \Exception $previousException The previous Exception (optional)
     */
    public function __construct($class, $obj, $previousException = null)
    {
        $this->class = $class;
        $this->obj = $obj;

        parent::__construct('Unable to convert given '.Exception::getType($obj).' to '.$class, $previousException);
    }

    /**
     * Prints the information useful for debugging
     * @see var_dump($convertException)
     */
    public function __debugInfo()
    {
        return [
            'previousException'=>$this->getPrevious(),
            'intendedClass'=>$this->class,
            'elementToConvert'=>$this->obj,
        ];
    }
}
