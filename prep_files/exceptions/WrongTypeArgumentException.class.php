<?php
/**
* Declare the class WrongTypeArgumentException
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   WrongTypeArgumentException.class
* @since        4.2
*/
namespace prep;

/**
 * Thrown when an argument does not match/implement the requested type when calling a PREP's method
 *
 * @since 4.2.0
 */
class WrongTypeArgumentException extends ArgumentException
{
    /**
     * Constructor
     * @param int        $index             The index of the mis argument
     * @param string     $name              The name of the missing argument
     * @param string     $expectedType      The type expected for the parameter
     * @param string     $givenType         The actual type provided
     * @param \Exception $previousException The previous Exception (optional)
     */
    public function __construct($index, $name, $expectedType, $givenType, $previousException = null)
    {
        parent::__construct(
            'Argument #'.$index.' "'.$name.'" is expected to be '.$expectedType.', '.$givenType.' given',
            $previousException
        );
    }
}
