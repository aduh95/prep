<?php
/**
* Declare the class ArgumentException
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   MissingArgumentException.class
* @since        4.2
*/
namespace prep;

/**
 * Thrown when an argument is missing when calling a PREP's method
 *
 * @since 4.2.0
 */
class MissingArgumentException extends ArgumentException
{
    /**
     * Constructor of the prep\MissingArgumentException
     * @param int            $index                 The index of the missing argument
     * @param string         $name                 The name of the missing argument
     * @param \Exception     $previousException  The previous Exception (optional)
     */
    public function __construct($index, $name, $previousException = null)
    {
        parent::__construct('Missing non optional argument #'.$index.' "'.$name.'"', $previousException);
    }
}
