<?php
/**
* Declare the class QueryFailedException
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   QueryFailedException.class
* @since        4.1
*/
namespace prep;

/**
 * Thrown when MySQL returns an unexpected result
 */
class QueryFailedException extends Exception
{
}
