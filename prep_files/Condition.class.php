<?php
/**
* Declare the class Condition, that extends Field, which uses trait  databaseMethodsTrait
*
* @author       aduh95
* @version      4.5.0
* @package      PREP
* @subpackage   Condition.class
* @since        3.0.1
*
*/
namespace prep;

use DatePeriod;

/**
 * Create conditions for prep\Where.
 */
class Condition extends Field implements ConditionInterface
{
    use conversionTrait;
    /*
     * For the object constructor, see the documentation of prep\Field's one.
     * Therefore, there is some more key in the argument $param that can be passed
     * @param array $param
        * keys documented in parent::__construct
        * @key 7:operator => ~string
        * @key 8:logicLink => ~string "AND" or "OR", default is "AND"
     */

    /**
     * String representation of the object
     * @return string A SQL fragment
     */
    public function __toString()
    {
        return (
            (!empty($this->param['logicLink']) && $this->param['logicLink']==='OR') ? ' OR ' : ' AND '
        ).self::getSQLValue();
    }

    /**
     * Outputs the value with operator
     *
     * Default value for the operator is TRUE.
     * Default value for the value is TRUE.
     * if value is:
         *bool: The field will be checked as a boolean
         *string or Field: if the operator is:
             * TRUE: outputs "="
             * FALSE: outputs "<>"
             * other : string value directly outputs (unprotected) (be aware to add spaces on string operators)
         * array: depends of the operator:
             * 'BETWEEN' or 'NOT BETWEEN' output properly
             * FALSE or empty value : outputs 'NOT IN'
             * either: outputs 'IN'
         * NULL: if the boolean value of the operator is:
             * TRUE: outputs 'IS NULL'
             * FALSE : outputs 'IS NOT NULL'
     *
     * @return string The SQL value for this condition
     */
    public function getSQLValue()
    {
        $operator = isset($this->param['operator']) ?
            (is_string($this->param['operator']) ? strtoupper($this->param['operator']) : !!$this->param['operator']) :
            true;
        if (is_string($operator) && in_array($operator[0], range('A', 'Z'))) {
            $operator= ' '.$operator.' ';
        }
        $return = $this->getProperties(0);

        switch (gettype($value = $this->param('value'))) {
            case 'NULL':
                return $return . ($operator ? ' IS NULL' : ' IS NOT NULL');

            case 'boolean':
                return (($operator xor $value) ? 'NOT ' : null).$return;

            case 'array':
                $value = (substr($operator, -7)==='BETWEEN') ?
                    $operator.' ' . self::implode($value, [$this,'safeVal'], ' AND ')
                :
                    $this->fctSQL($operator && 'NOT IN'!==$operator ? 'IN' : 'NOT IN', $value);
                return $return . ' ' . $value;

            default:
                if ($value instanceof DatePeriod) {
                    return $return.($operator && 'NOT BETWEEN'!==$operator ? ' BETWEEN ' : ' NOT BETWEEN ').
                        $this->safeVal($value->getStartDate()).
                        ' AND '.
                        $this->safeVal($value->getEndDate());
                } else {
                    return $this($operator);
                }
        }
    }
}
