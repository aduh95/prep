# PREP

Prep project is a PHP project that aims to make database interaction easier and
lighter for the developers. Since it will be faster to write correctly your
queries, and easier to read (especially for someone not aware of SQL syntax).
You won't *need* to know SQL to make your own queries (all the main SQL keywords
and the anti-injection mechanisms of data is build by PREP), but knowledge may
help you understand if an error occurs.

## Installation

#### Using Composer

If you're using [Composer](https://getcomposer.org) in your project, you can
type the following command line:

```bash
composer require prep/prep
```

And it's done! If you haven't already, include the `vendor/autoload.php` file to
use Prep.

Or you just add this lines in your `composer.json` file:

```json
{
  "require":
  {
    "prep/prep": "~4.4"
  }
}
```

Then run `composer install` or `composer update`, and that's it!

#### Using the PHAR

You can download the latest version of PREP in the [Download section](https://bitbucket.org/aduh95/prep/downloads) and unzip it. Then, you will have to add the following line on the beginning of your scripts:

```php
<?php
require_once 'Prep.phar';
?>
```

And that's it!

*If you are using an IDE, the PHAR archive is stripped of every thing that is not PHP code, so your IDE won't find the documentation. You should add the multi-files code to your dev environment.*

#### Using Git

You can clone the project from Git `git clone https://bitbucket.org/aduh95/prep.git` and then, include the autoload file from your PHP file:

```php
<?php
require_once 'PREP.autoload.php';
?>
```

## How to use it?

This project has been made to make SQL queries intuitive. You don't have to
worry about SQL injections, PREP does it for you, and makes your job less
complicated when it comes to error handling - all you have to do is to catch an
Exception.

```php
<?php
try {
    Prep::insert('tableName', ['name'=>$_POST['name'], 'email'=>$_SESSION['email'], 'permissions'=>PERMISSIONS\ADMIN]);
} catch (prep\Exception $e) {
    echo 'Here you can inform your users an error inform';
    // You may also execute any other code to inform yourself to correct that error
    error_log($e->getMessage());
}
?>
```

As you can read, this code makes an insertion in the `tableName` table of your
database, specifying a `name`, an `email` and a `permission` level. And don't
worry about SQL injections, it has been taking cared of!

An error keeps occurring and you don't know why? Just use the `debug_` prefix on
your method, and you know what it is about:

```php
<?php
try {
    Prep::debug_insert('wrongTableName', ['name'=>$_POST['name'], 'email'=>$_SESSION['email'], 'permissions'=>2]);
} catch (Exception $e) {
    exit('An error occurred, sorry :/');
}
?>
```

And you'll get something like this:

```text
C:\prep_project\source\prep.class.php:340:
object(prep\Query)[7]
  public 'PDOStatement::rowCount()' => int 0
  public 'SQL_query' => string 'INSERT INTO  `wrongTableName` (`name`,`email`,`permissions`) VALUES (?,?,2)' (length=75)
  public 'parameters' => 
    array (size=2)
      0 => string 'aduh95' (length=6)
      1 => string 'test@exemple.net' (length=16)
  public 'MySQL_errorInfo' => 
    array (size=3)
      0 => string '42S02' (length=5)
      1 => int 1146
      2 => string 'La table 'database.wrongtablename' n'existe pas' (length=47)
```

You can see that the table name is misspelled (it is written in French because
my MySQL server is french).

## Examples and manual

Please see the [**wiki of the
project**](https://bitbucket.org/aduh95/prep/wiki/Home), it will teach you all
you need to know *(and even more)*.

## Requirements

It requires **PHP 5.6 or higher** as of version 3.8. The SQL syntax generated
has been tested for **MySQL 5.5 or higher**.

## Where does it come from?

The idea of PREP came when I discovered jQuery: a library thought for the
developer. I knew something like that for the database interaction was missing.
I wanted to create a project that was easy to write and to read, easy to
remember and secured.

I learned a lot writing all those classes, made a lot of mistakes. If I let any
in the code, please tell me! If you want to contribute to or if you have any
suggestion about the project, do not hesitate to contact me.

**Thank you for reading and I hope you'll have *fun* using my tool.**
