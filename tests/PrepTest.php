<?php
/**
 * Created by PhpStorm.
 * @author Antoine
 * Date: 15/07/2016
 * Time: 18:04
 */

namespace prep;
use PHPUnit\Framework\TestCase;
use Prep;

class PrepTest extends TestCase
{

    public function testSimpleSelectInit()
    {
        // Disabling database connection requirement
        Prep::$PDO = new class extends PDO {
            function __construct()
            {
            }
        };
        $select = Prep::selectInit('tableName');

        $this->assertEquals(
            'SELECT * FROM  `tableName` AS `'.Prep::MAIN_TABLE.'`',
            $select->getQuery()
        );
    }

    /**
     * @depends testSimpleSelectInit
     */
    public function testSelectInitWithAField()
    {
        $select = Prep::selectInit('tableName', 'fieldName');

        $this->assertEquals(
            'SELECT `'.Prep::MAIN_TABLE.'`.`fieldName` FROM  `tableName` AS `'.Prep::MAIN_TABLE.'`',
            $select->getQuery()
        );
    }

    /**
     * @depends testSimpleSelectInit
     */
    public function testSelectInitWithSeveralField()
    {
        $select = Prep::selectInit('tableName', array('fieldName1', 'fieldName2'));

        $this->assertEquals(
            'SELECT `'.Prep::MAIN_TABLE.'`.`fieldName1`,`'.Prep::MAIN_TABLE.'`.`fieldName2` FROM  `tableName` AS `'.Prep::MAIN_TABLE.'`',
            $select->getQuery()
        );
    }
}
