<?php
/**
 * Created by PhpStorm.
 * @author Antoine
 * Date: 15/07/2016
 * Time: 18:04
 */

namespace prep;
use PHPUnit\Framework\TestCase;
use DateInterval;
use DatePeriod;

class ConditionTest extends TestCase
{

    public function testSimpleCondition()
    {
        $condition = new Condition([
            'table'=>'table',
            'column'=>'field',
            'value'=>0,
        ]);

        $this->assertEquals(
            '`table`.`field`=0',
            $condition->getSQLValue()
        );
    }

    /**
     * @depends testSimpleCondition
     */
    public function testConditionWithDatePeriod()
    {
        $period = new DatePeriod(
            date_create('2016-05-28'),
            new DateInterval('P1D'),
            date_create('2016-06-28')
        );
        $condition = new Condition([
            'table'=>'table',
            'column'=>'field',
            'value'=>$period
        ]);

        $this->assertEquals(
            '`table`.`field` BETWEEN \'2016-05-28 00:00:00\' AND \'2016-06-28 00:00:00\'',
            $condition->getSQLValue()
        );
    }
}
