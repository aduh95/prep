# Prep CHANGELOG

### v4.5.0(2017-06-07)
- In the `prep\databaseMethodsTrait`
    + Add DateTimeInterface objects support
- In the `prep\Condition` class
    + Add support for DatePeriod value type
- In the `prep\Field` class
    + Patch a bug with PHP 5.6
- In the `prep\SQLQuery` class
    + Patch a bug on setLimit when incorrect limit is given

#### v4.4.2(2017-04-15)
- In the `prep\Condition` class
    + Patch a bug with `logicLink` argument

#### v4.4.1(2016-07-20)
- In the `prep\ArgumentException` class
    + Patch a bug with snake_case argument names

### v4.4.0(2016-07-18)
- Improve code quality
    + Most of the PSR-2 recommendation have been followed (some cannot be implemented without losing backward compatibility)
    + Many english mistakes have been corrected
- Arguments can now be named in camelCase (or even CamelCase) or in snake_case (or even in SNAKE_CASE)

#### v4.3.5(2016-06-01)
- In the `prep\SQLQuery` class
    + Bug patch: when using `prep\DirectSQL` as tables, group_by or order_by; protected values were ignored, and caused sql query failure

#### v4.3.4(2016-06-01)
- In the `Prep` class
    + Bug patch that causes SQL query to fail on `Prep::updateOne` and `Prep::deleteOne`

#### v4.3.3(2016-05-29)
- In the `Prep` class
    + Bug patch when joining a table with ambiguous ID field with `Prep::selectOne`
- In the `prep\databaseMethodsTrait` trait
    + Bug patch when using `safeVal` on an object without __invoke method

#### v4.3.2(2016-04-19)
- In the `prep\Condition` class
    + Bug patch when an array was passed as value
- In the `prep\databaseMethodsTrait` trait
    + Bug patch when an array was passed as arguments for a SQL function
- In the `prep\DirectSQL` class
    + Bug patch in the constructor, protected value was not accepted
- In the `prep\CustomQuery` class
    + TypeError is thrown if third argument is not an *array*
- In the `Prep` class
    + the `values` argument is automatically cast to *array* in the `query` method

#### v4.3.1(2016-04-17)
- In the `prep\Select` class
    + Bug patch with the `count` method
- In the `Prep` class
    + Bug patch when `order_by` parameter was an array in a selection
- In the `prep\Column` class
    + Bug patch in the string representation when no table provided

### v4.3.0(2016-04-13)
- In the `prep\Field` class
    + Patch a bug when a non array single argument was provided into an array to `prep\Field::convert` or `prep\Condition::convert`
- In the `prep\conversionTrait` trait
    + Patch a bug when using the `func_get_args` function to convert
- In the `prep\SQLQuery` class
    + Add the `__toString` method
- In the `prep\ArgumentException` class
    + The class is no longer *abstract*
    + The argument checked by the `checkArgs` method are now case insensitive
- Add the `prep\WhereInterface` interface, that extends the `prep\PrepMethodsInterface`
- In the `prep\Where` class
    + The class now implements `prep\WhereInterface` instead of `prep\PrepMethodsInterface`
- In the `Prep` class
    + You can have now custom `Where` object, if it implements the `prep\WhereInterface` interface
- Optimisation

### v4.2.0(2016-04-08)
- Improve the error messages (the more explicit, the better)
    + When a `prep\Exception` is thrown, the actual line where the error has been made is thrown, and not the internal line
    + When a file is missing, the real expected path is output (before, it was the last checked)
    + When a MySQL error happens, the error message is automatically displayed
- In the file organization
    + Dedicated directories to store the different source files
    + Rename the files to make it match the names of the classes and the name of the files
- In the `prep\Exception` class (and its sub-classes)
    + The user line that throw the `Exception` is now retrieved rather than the internal line (aka instead of having a message that says an `Exception` has been thrown from a PREP file, it retrieves the user call to the PREP function/method)
    + The first argument `$method` is removed
    + Add `gettype` static method to retrieve easier the type or the class name of a parameter
- In the `prep\ArgumentException` class
    + This class is now *abstract*
    + In the `checkArgs` method, you can now specify a scalar type for the arguments to match
    + It's the parent class of two new `Exception` class : `prep\MissingArgumentException` and `prep\WrongTypeArgumentException`
- In the `prep\MySQLErrorException` class
    + Add the `getMySQLErrorCode` method
    + Add the `getMySQLErrorInfo` method
- Add the interfaces `prep\ColumnInterface`, `prep\ConditionInterface`, `prep\FieldInterface`, `prep\PrepMethodsInterface`, `prep\TableInterface`
- Rename `prep\db_methods` trait to `prep\databaseMethodsTrait`
- Rename `prep\prep_methods` trait to `prep\prepMethodsTrait`
- In the `prep\prepMethodsTrait` Trait
    + The `implode` method is now static
- Add the `prep\conversionTrait` trait
    + Centralize all the conversion methods
    + All the `public static convert` methods have been removed from the classes that use this trait
- Add the `prep\EmptyTable` class
- In the `prep\DirectSQL` class
    + Add the `getNickName` method to patch a bug when the object is used as a prep\Table
    + The class now implements the interfaces
- In the `prep\Field` class
    + Remove the table internal parameter, now uses `prep\Column` only
    + Rename `convertToColumn` to `getColumn`
- In the `prep\Condition` class
    + Rename the `strval` method to `getSQLValue`
- In the `prep\Column` class
    + Rename the `convertToField` method to `getField`
    + Rename the `convertToCondition` method to `getCondition`
- In the `Prep` class
    + Patch a bug when the `table` parameter was given as a `prep\DirectSQL`, `NULL` or an empty **string** (Issue #3)
    + Change the visibility of `Prep::getCallingMethod` from *private* to *public*


### v4.1(2016-03-05)
- Better Exception handling
    + Add the `prep\ArgumentException`, `prep\MySQLErrorException`, `prep\QueryFailedException`, `prep\ConvertException` classes that extend `prep\Exception`
    + Delete the `prep\Exception::get_args` method
    + Adapt the classes to throw the relevant Exception when an error occurs
- In the `Prep` class
    + Fix issue #1: Exception does not make sense when calling `Prep::connect` without argument
    + Delete the `Prep::throwException` method, that make stack trace dirty
    + Internal changes
- In the `prep\Column` class
    + Fix a bug with the `convert` method, when a non object was provided as parameter
- In the `prep\Select` class
    + Fix a bug with the `count` method, causing `GROUP BY` to fail
- In the `prep\CustomQuery` class
    + Patch a bug occurring when prepared parameters were bound


#### v4.0.2(2016-02-20)
- In the `Prep` class
    + Fix a bug with PHP 7
    + Fix a bug with the `deleteAll` and `updateAll` methods

#### v4.0.1(2016-02-18)
- In the `prep\PDO` class
    + Modify constructor
    + Added `quotes` method
    + Change returned values for the `beginTransaction`, `commit` and `rollBack` from *void* to **bool**
- In the `prep\db_methods` Trait
    + Delete the `prep\db_methods::quotes` method that is implemented in the `prep\PDO` class
- In the `Prep` class
    + Adapt `Prep::connect` to the new `prep\PDO` constructor
- In the `prep\SQLQuery` class
    + Rename the `getValues` method to `getVal`
- In the `prep\DirectSQL` class
    + Rename the `orderby_str` to `orderByStr` for compatibility with other classes
- In the `prep\Field` and the `prep\Table` classes
    + Use `prep\PDO::quotes` instead of `prep\db_methods::quotes`

## v4.0 (2016-02-15)
- Rename the `prep\Commun` class to `prep\SQLQuery`
- Rename methods for more coherence:
    + `prep\prep_methods::is_empty` `isEmpty`
    + `prep\SQLQuery::get_query` `getQuery`
    + `prep\SQLQuery::get_values` `getValues`
    + `prep\SQLQuery::limit` `setLimit`
    + `prep\Field::order_by_str` `orderbyStr`
    + `prep\Query::add_champ` `addField`
    + `prep\db_methods::alias_it` `getAlias`
- Get ride of index.php, use autoload now
- Add the `__debugInfo` magic method in `prep\SQLQuery`
- Delete `prep\SQLQuery::get_error`, since the class implements the `__debugInfo` magic method
- Delete `prep\SQLQuery::force_query`, since `prep\CustomQuery` do its job
- Add the `Prep::SQL` method and the `prep\DirectSQL` class to write direct SQL in a query
- Add the `prep\PDO` class to handle multi-step transactions
- Change the arguments of the `Prep::one` method to match the arguments of `Prep::select` and `Prep::selectOne`
- Add the methods `Prep::updateAll` and `Prep::deleteAll`
- Delete the `prep\SQLQuery::$query` property, obsolete since prep\CustomQuery is out
- Change the `public` `prep\SQLQuery::$bdd` property to `protected` `$pdo`

-------------------------

### v3.8 (2015-12-30)
- **PHP 5.6 or higher is required**
- Patch a bug with order_by and group_by
- Rename `group_by_str` to `order_by_str` in `prep\Field`
- Optimisation of `prep\Column` converted to `prep\Field`
- Add `prep\CustomQuery` to build custom queries
- Add Prep::query method using `prep\CustomQuery`
- Add `get_args` method of the trait `exceptions` to `prep\Exception`
- Delete trait `exceptions`
- Patch a bug with `Prep::insert` and defaultArray
- Patch a bug appearing when trying to debug with a transaction already begun
- Add a fetch style argument to `Prep::selectOne`
- Add a constant `Prep::MAIN_TABLE` used as alias for the main table is a selection
- Less information are given by `prep\Commun::get_error`, a lot of it used to be irrelevant
- Patch a bug with `prep\Query` with insertion or update of boolean values
- Add class constants for `prep\Query`, constructor now excepts an integer for $type, instead of a string


#### v3.7.3 (2015-11-19)
- Patch a bug with order_by
- Add the group_by_str method in `prep\Field`
- Add a "field_ID" argument to Prep::*One, in case the selection is not on ID
- Add the class `prep\Exception` for custom exceptions
- Delete the reset methods in `prep\Commun`, `prep\Query` and `prep\Select` classes

#### v3.7.2 (2015-10-13)
- Throws Exception when empty selection occurs using `Prep::selectAll` or `Prep::selectOne`
- Throws `Exception` when query failed using `Prep::updateOne` or `Prep::deleteOne`
- Throws `Exception` when insertion failed using `Prep::insert`
- Suppression of the debug methods on prep.min

#### v3.7.1 (2015-10-07)
- Add limit=1 to `Prep::one`
- Patch a bug when throwing Exceptions
- Optimisation of `prep\exceptions::get_args`

### v3.7 (2015-10-05)
- Patch a bug when passing a `prep\Field` to `prep\Query::add_champ`
- Patch a bug with `Prep::updateOne` and `Prep::deleteOne` methods
- Patch a bug raising an `Exception` when using `Prep::update`
- Rename the `gest_args` trait `exceptions` for more clarity
- Patch a bug of wrong arguments asked when calling Prep::delete* methods
- Add a "defaultArray" argument to Prep::insert* methods
- Creating the "prep_methods" trait; moving param, getRecursiveVal, getVal, is_empty and implode methods into it
- Modify the combinations of traits
- Delete Commun::$executabled


#### v3.6.2 (2015-10-01)
- Patch a bug with `defaultSettings` parameter in `prep\Field` and `prep\Condition` classes

#### v3.6.1 (2015-09-29)
- Patch a bug sent an error when no `prep\Table` was assigned to a `prep\Field` or a `prep\Condition`
- Patch a bug when selecting all fields in a table
- Minor fixes

### v3.6 (2015-09-28)
- Add the `Prep::selectIDs` method
- `prep\db_methods::param` now accept array of parameter to make easier multi-changes
- Replace `prep\Where::defaultTable` public propriety by `prep\Where::defaultSettings`
- Patch a bug occurring when passing a `prep\Where` object to `Prep::init`
- Add an argument "defaultSettings" to `prep\Field` and `prep\Condition` constructors


### v3.5 (2015-09-19)
- Add arguments to `Prep::selectAll`
- Minor optimisations
- Patch many documentation fails


### v3.4 (2015-09-18)
- You can store a `PDO` object in `Prep::$PDO`
- The `PDO` argument is now optional for every methods of `Prep` if `Prep::$PDO` is set
- The `PDO` argument is now the last argument provided, used to be the first one
- Patch a bug with Prep::select* order_by

###### Comparing PREP 3.4 and 2.3
 - The results show that PREP 3.4 is 96% as fast as PREP 2.3 on a local machine
 - (which means PREP 3.4 is about 4.1% slower than PREP 2.3)
 - Try with a thousand selection with each and compare durations at the end


#### v3.3.4(2015-09-17)
- Patch a bug happening when joining `prep\Table` objects
- Every classes that do not extend `prep\Commun` had now a method "convert"
- Patch calling `Prep::connect` with an array of parameters

#### v3.3.3(2015-09-17)
- Patch a bug in `Prep::one`, `PDOStatement` was returned instead of array

#### v3.3.2 (2015-09-16)
- Better handling of `Exception`
- Improvement of the connection to the database
- Remove the function `prep\mysqlConnect`, use `Prep::connect` instead

#### v3.3.1 (2015-09-15)
- Patch a bug when no Table was provided to `prep\Column` constructor
- Patch a bug that made no `prep\Table` assigned to `$fields` in `Prep::init`
- Patch a bug on $where in `Prep::init`
- `Exception`s are raised when MySQL errors happen
- `prep\Select` method count is now is compatible with group_by and rollup

### v3.3 (2015-09-14)
- Creation of the trait `gest_args`
- Generalisation of Exceptions when an error happens
- Most of the functions with parameters use the trait `gest_args`
- Many bugs patches, especially about NULL values not passed to MySQL


### v3.2 (2015-09-13)
- The debug information are now available using the prefix "debug_" for all the method of Prep except connect
- The `Prep::error` is removed


### v3.1 (2015-09-13)
- Creation of the class `\Prep` (outside of the namespace) to replace the *.func.php
- Homogenize the order of the arguments in the `Prep`'s methods
- Exception are raised when not enough arguments are provided
- Every `Prep` methods may be called with an array, so you may avoid to set the disabled arguments to false


## v3.0 (2015-09-12)
- **PHP 5.4 or higher is required**
- Creation of the namespace `prep`, new names for the classes (`prep_class`=>`prep\Commun`, `req_prep`=>`prep\Query`, `select_prep`=>`prep\Select`)
- Introduction of new classes (`prep\Field`, `prep\Table`, `prep\Column`)
- Creation of the trait `db_methods` ==> you need PHP >=5.4
- Improve the preparation of `value`, the numeric and `NULL` values are directly output
- Integration of `PrepWhere` in the namespace `prep` as `prep\Where` and `prep\Condition`
- Search methods (add_where*) are deleted, use methods of `prep\Where` instead
- `Select::alias` is deleted, use `Field::param('alias')` instead
- `Query::query` is deleted, its features are integrated in the constructor
- Add an argument to `Commun::exec` method, useful to `force_query`
- Begin a transaction when calling the `Commun::get_error` method


-------------------------


### v2.3 (04/2015)
- `PrepWhere` class basic compatibility

### v2.2 (08/2014)
- add `get_error` method

### v2.1 (07/2014)
- Optimisations


## v2.0  (07/2014)
- Separate `select_prep` and `req_prep`
- Add the abstract class `prep_class`
- Create user functions


-----------------------

## v1.0 (07/2014)
- Initial release
