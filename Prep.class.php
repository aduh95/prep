<?php
/**
 * Main class of the API
 *
 * Here you find the public methods you can use to create SQL queries. You can
 * find all the documentation you need on the Git repo's wiki. Be aware that the
 * methods are "static", and contain optional arguments that cannot be
 * represented on the following documentation using the current PHPDoc Standard.
 * All the following methods won't change until a major release
 *
 * @author       aduh95
 * @version      4.5.0
 * @package      PREP
 * @subpackage   Prep.class
 * @since        3.1.0
 *
 * @api
 * @link https://bitbucket.org/aduh95/prep/wiki/Home Documentation of PREP
 *
 * @method static prep\Select selectInit(
 *  prep\TableInterface $table,
 *  prep\FieldInterface[]|prep\FieldInterface $fields,
 *  prep\Where|prep\Condition[] $where,
 *  prep\TableInterface[]|prep\TableInterface $join,
 *  mixed $orderBy,
 *  mixed $limit,
 *  PDO $PDO
 * )
 * @method static prep\Query  insertInit(
 *  prep\TableInterface $table,
 *  prep\FieldInterface[] $fields,
 *  array $defaultArray,
 *  PDO $PDO
 * )
 * @method static prep\Query  updateInit(
 *  prep\TableInterface $table,
 *  prep\FieldInterface[] $fields,
 *  array $wheres,
 *  int $limit,
 *  PDO $PDO
 * )
 * @method static prep\Query  deleteInit(
 *  prep\TableInterface $table,
 *  array $where,
 *  int $limit,
 *  PDO $PDO
 * )
 * @see init
 *
 *
 * @method static PDOStatement  select(
 *  prep\TableInterface $table,
 *  prep\FieldInterface[]|prep\FieldInterface $fields,
 *  mixed $where,
 *  array $join,
 *  mixed $orderBy,
 *  mixed $limit,
 *  PDO $PDO
 * )
 * @method static int insert(
 *  prep\TableInterface $table,
 *  prep\FieldInterface[]|prep\FieldInterface $fields,
 *  array $defaultArray,
 *  PDO $PDO
 * )
 * @method static int update(
 *  prep\TableInterface $table,
 *  prep\FieldInterface[]|prep\FieldInterface $fields,
 *  mixed $where,
 *  int $limit,
 *  PDO $PDO
 * )
 * @method static int delete(
 *  prep\TableInterface $table,
 *  mixed $where,
 *  int $limit,
 *  PDO $PDO
 * )
 * @see prep
 *
 *
 * @method static int|array selectCount(
 *  prep\TableInterface $table,
 *  array $where,
 *  array $group_by,
 *  bool $rollup,
 *  PDO $PDO
 * )
 * @see count
 *
 * @method static array     selectIDs(
 *  prep\TableInterface $table,
 *  array $where,
 *  array $left_join,
 *  mixed $orderBy,
 *  mixed $limit,
 *  PDO $PDO,
 *  ~prep\Column $fields,
 *  int $style,
 *  mixed $argument,
 *  array $ctorArgs
 * )
 * @see ids
 *
 *
 * @method static array selectOne(
 *  prep\TableInterface $table,
 *  int $ID,
 *  prep\FieldInterface[]|prep\FieldInterface $fields,
 *  array $join,
 *  mixed $orderBy,
 *  string $field_ID,
 *  PDO $PDO,
 *  int $fetch_style
 * )
 * @method static bool  updateOne(
 *  prep\TableInterface $table,
 *  int $ID,
 *  prep\FieldInterface[]|prep\FieldInterface $fields,,,
 *  string $field_ID,
 *  PDO $PDO
 * )
 * @method static bool  deleteOne(
 *  prep\TableInterface $table,
 *  int $ID,,,,
 *  PDO $PDO,
 *  string $field_ID
 * )
 * @see one
 *
 *
 * @method static array selectAll(
 *  prep\TableInterface $table,
 *  prep\FieldInterface[]|prep\FieldInterface $fields,
 *  array $where,
 *  array $left_join,
 *  mixed $orderBy,
 *  mixed $limit,
 *  PDO $PDO,
 *  int $style,
 *  mixed $argument,
 *  array $ctor_args
 * )
 * @method static int   updateAll(
 *  prep\TableInterface $table,
 *  prep\FieldInterface[]|prep\FieldInterface $fields,
 *  array $where,,
 *  PDO $PDO
 * )
 * @method static int   deleteAll(
 *  prep\TableInterface $table,
 *  array $where,,
 *  PDO $PDO
 * )
 * @see all
 */
abstract class Prep
{
    /**
     * @var PDO $PDO The object that contains the default database connection used
     */
    public static $PDO;

    /**
     * @var string  $method_called The name of the method called
     */
    protected static $method_called;

    /**
     * @var bool    $debug         Defines if the debug mode is on or not
     */
    protected static $debug=false;


    /**
     * @var string The alias used by Prep for the main table in the selection query
     *              The joined tables default alias is this alias concats with a number
     */
    const MAIN_TABLE = 't';

    /**
     * @var string The current version of Prep
     */
    const VERSION = '4.5.0';

    /**
     * @var int The version of Prep disposed like this: $major_version * 10000 + $minor_version * 100 + $release_version
     */
    const VERSION_ID = 40500;



    /**
     * The class has no constructor, but that line is necessary to avoid prep to be interpreted as a constructor
     */
    abstract public function __construct();

    /**
     * Converts the methods called into the actual one.
     *
     * @param string    $name The name of the static method called
     * @param array     $args The arguments provided
     *
     * @return mixed
     * @throws prep\Exception If the called method is not defined.
     */
    public static function __callStatic($name, $args)
    {
        $query = substr(self::$method_called = $name, 0, 6);
        $method = 'self::'.(strlen($name)>6 ? substr($name, 6) : 'prep');

        if ($query==='debug_') {
            self::$debug=true;
            return call_user_func_array($method, $args);
        }

        return call_user_func($method, $query, $args);
    }

    /**
     * Builds a PDO object to interact with the MySQL database
     *
     * @api
     *
     * @param string 0:$db_name: name of the data base
     * @param string 1:$user: user name
     * @param string 2:$mdp: password
     * @param string 3:$host: IP or URL of the server (default is localhost)
     *
     * @return prep\PDO
     * @throws prep\Exception If the connection fails
     */
    public static function connect()
    {
        $args = prep\ArgumentException::checkArgs([
            ['DBname', 'user', 'password', 'host'],
            ['DBname', 'user', 'password',],
            [3=>'localhost']
            ], func_get_args());
        extract($args);

        try {
            return new prep\PDO($DBname, $user, $password, $host);
        } catch (PDOException $e) {
            throw new prep\Exception('MySQL connection failed, PDO returned "'.$e->getMessage().'"', $e);
        }
    }

    /**
     * Executes a custom SQL query
     *
     * @api
     *
     * @param string    0:query The SQL statement
     * @param array     1:values The values to insert in the prepared query (@see PDO::prepare)
     * @param PDO       2:PDO The PDO object referring to the database
     *
     * @return PDOStatement
     * @throws prep\MySQLErrorException      If SQL returns an error
     * @throws prep\MissingArgumentException If a non optional argument is omitted
     */
    public static function query()
    {
        $args = prep\ArgumentException::checkArgs([
            ['query', 'values', 'PDO'],
            ['query'],
            ['PDO'=>self::$PDO, 'values'=>array()]
            ], func_get_args());
        extract($args);

        $prep = new prep\CustomQuery($PDO, $query, (array)$values);
        if(self::$debug) {
            var_dump($prep);
            exit;
        }
        $return = $prep->exec();
        prep\MySQLErrorException::checkError($return);
        return $return;
    }

    /**
     * Creates a prep\DirectSQL object that allows you to input directly some SQL code in your query
     *
     * @api
     * @see prep\DirectSQL::__construct It is an alias (or a shortcut)
     *
     * @param string        :$sql   The SQL code you want to inject
     * @param array|mixed   :$values The values to protect, that will replace the ? in $sql,
     *                              as an array or multiple arguments
     *
     * @return prep\DirectSQL
     */
    public static function SQL()
    {
        return new prep\DirectSQL(...func_get_args());
    }

    /**
     * Initiates a prep\Query or prep\Select object
     *
     * @param string $query
     * @param array $args
         * @key 0:"table"=> prep\TableInterface
         * ... depends of $query, check documentation
     *
     * @return prep\Query|prep\Select
     * @throws prep\Exception If $query is invalid
     * @throws prep\MissingArgumentException If one of the required argument is missing
     * @throws prep\WrongTypeArgumentException
     */
    protected static function init($query, $args)
    {
        $arg_keys = ['table', 'fields'];
        $required_args = ['table'];
        $default_val = array();
        $expected_argTypes = ['prep\Table'];
        switch ($query) {
            case 'select':
                $arg_keys+= [2=>'where', 'join', 'orderBy', 'limit'];
                break;

            case 'insert':
                // $arg_keys = ['table', 'fields'];
                $arg_keys[] = 'defaultArray';
                $required_args[] = 'fields';
                break;

            case 'update':
                $arg_keys+= [2=>'where', 'limit'];
                $required_args[] = 'fields';
                $default_val[3] = 1;
                break;

            case 'delete':
                $arg_keys[1]= 'where';
                $arg_keys[]= 'limit';
                $default_val[2] = 1;
                break;

            default:
                throw new prep\Exception('Unknown method');
        }

        // default PDO object
        if (is_object(self::$PDO) && is_a(self::$PDO, 'PDO')) {
            $default_val[count($arg_keys)] = self::$PDO;
        } else {
            $required_args[] = 'PDO';
        }
        $arg_keys[] = 'PDO';

        $args = prep\ArgumentException::checkArgs([$arg_keys, $required_args, $default_val, $expected_argTypes], $args);
        extract($args);

        $prep = $query==='select' ?
            new prep\Select($PDO, $table) :
            new prep\Query($PDO, constant('prep\Query::'.strtoupper($query)), $table);

        if ($query==='select') {
            $alias = 0;
            if(!$table->param('alias')) {
                $table->param('alias', self::MAIN_TABLE);
            }

            // Starting with $join to get the Table objects
            if (!empty($join)) {
                if(is_array($join)) {
                    foreach ($join as $join_table=>$join_elem) {
                        $prep->addJoin(is_numeric($join_table) ?
                            $join_elem :
                            new prep\Table([
                                'name'=>$join_table,
                                'join'=>'LEFT',
                                'alias'=>self::MAIN_TABLE.$alias++,
                                'ON'=>new prep\Where(['column'=>'ID', 'value'=>new prep\Column($join_elem, $table)])
                                ])
                            );
                    }
                } else {
                    $prep->addJoin($join);
                }
            }

            if (!empty($fields)) {
                if (is_array($fields)) {
                    foreach($fields as $key=>$values) {
                        if (is_numeric($key)) {
                            // [(string or Field) $field1, (string or Field) $field2, ...]
                            if (is_array($values)) {
                                $prep->addField(new prep\Field($values));
                            } else {
                                $prep->addField($values, $table);
                            }
                        } elseif (is_array($values)) {
                            // [(string) $table1 => [(string) $field1, ...], ...]
                            array_reduce(
                                $values,
                                function ($pv, $cv) {
                                    $pv[0]->addField($cv, $pv[1]);
                                    return $pv;
                                },
                                [$prep, $prep->getTable($key)]
                            );
                        } else {
                            // [(string) $table1 =>(mixed) $value, ...]
                            $prep->addField($values, $prep->getTable($key));
                        }
                    }
                } else {
                    // (string or Field) $field
                    $prep->addField($fields);
                }
            }

            if (!empty($orderBy)) {
                if (is_array($orderBy)) {
                    foreach ($orderBy as $key=>$value) {
                        if (is_numeric($key)) {
                            // [(string or Field) $orderBy_field1, ...]
                            $prep->orderBy(is_string($value) ? new prep\Column($value, $table) : $value);
                        } else {
                            //  [(string) $field => (bool) $desc, ...]
                            $prep->orderBy(new prep\Field(['column'=>$key, 'orderBy'=>$value?'DESC':'ASC']));
                        }
                    }
                } else {
                    // (Field|string) $field
                    $prep->orderBy(is_string($orderBy) ? new prep\Column($orderBy, $table) : $orderBy);
                }
            }
        } elseif (!empty($fields) && is_array($fields)) {
            foreach ($fields as $key=>$value) {
                if(is_numeric($key)) {
                    $prep->addField(
                        $value,
                        is_string($value) && !empty($defaultArray) && array_key_exists($value, $defaultArray) ?
                            $defaultArray[$value] :
                            $value
                    );
                } else {
                    $prep->addField($key, $value);
                }
            }
        }

        if (!empty($where)) {
            if(is_array($where)) {
                foreach ($where as $column => $value) {
                    $prep->where->addCondition(
                        is_numeric($column) ? $value : ['column'=>[$column, $table], 'value'=>$value]
                    );
                }
            } elseif (is_object($where) && $where instanceof prep\WhereInterface) {
                $prep->where = $where;
            } else {
                $prep->where->addCondition($where);
            }
        }

        if(!empty($limit)) {
            $prep->setLimit($limit); // (int) $max_number_of_results
        }

        if (self::$debug) {
            var_dump($prep);
            exit;
        }

        return $prep;
    }

    /**
     * Executes the MySQL query generated
     * @param --@see init--
     * @param --@see init--
     * @return PDOStatement|int
     * @throws prep\MySQLErrorException When a SQL error occurred
     * @throws prep\QueryFailedException If the insertion failed
     */
    protected static function prep($query, $args)
    {
        $return = self::init(...func_get_args())->exec();
        prep\MySQLErrorException::checkError($return);
        if ($query=='insert' && !$return->rowCount()) {
            throw new prep\QueryFailedException('Insertion failed');
        }
        return $query==='select' ? $return : $return->rowCount();
    }

    /**
     * Executes the query for only one statement
     *
     * @param string $query
     * @param array $args
         * @key 0:"table"=> prep\TableInterface
         * @key 1:"ID"=> int
         * @key 2:"fields"=> mixed
         * @key 3:"join"=> --@see prep--
         * @key 4:"orderBy"=> --@see prep--
         * @key 5:"PDO"=> PDO
         * @key 6:"field_ID"=> string (default is 'ID')
         * @key 7:"style"
     *
     * @return array | bool
     * @throws prep\Exception                   If the method is used for insertion
     * @throws prep\QueryFailedException        If the selection is empty or if the query failed
     * @throws prep\WrongTypeArgumentException  If one of the parameter does not match the type expected
     * @throws prep\MissingArgumentException    If one of the non optional argument is omitted
     */
    private static function one ($query, $args)
    {
        if ($query=='insert') {
            throw new prep\Exception('Unknown method');
        }

        $args = prep\ArgumentException::checkArgs(
            [
                ['table', 'ID', 'fields', 'join', 'orderBy', 'PDO', 'fieldID', 'style'],
                ['ID'],
                [6=>'ID', PDO::FETCH_BOTH],
                [1=>'integer', 6=>'string']
            ], $args);
        $args['where'] = array(
            new prep\Condition($query==='select' ? self::MAIN_TABLE : null, $args['fieldID'], $args['ID'])
        );
        $args['limit'] = 1;

        $return = self::prep($query, $args);

        $return = $query==='select' ? $return->fetch($args['style']) : !!$return;

        if (!$return) {
            throw new prep\QueryFailedException('ID not found in that table');
        }

        return $return;
    }

    /**
     * Only for selection : returns an array with all the result got from MySQL.
     * Only for updating or deleting : returns the number of rows affected
     * @param string $query --@see init--
     * @param array $args
         * keys 0..6 --@see init--
         * @key 7:"style"=> int Please see http://php.net/manual/en/pdostatement.fetchall.php for more information
         * @key 8:"argument"=> mixed
         * @key 9:"ctor_args"=> array
     * @return array
     * @throws prep\Exception If the query is meant for insertion
     * @throws prep\QueryFailedException If the selection is empty or no row has been affected
     */
    private static function all ($query, $args)
    {
        if ($query==='insert') {
            throw new prep\Exception('Unknown method');
        }
        $selection = $query==='select';

        if(!$selection) {
            count($args)===1 && is_array($args[0]) ? $args[0]['limit']=false : $args+=['limit'=>false];
        }

        $prep = self::prep($query, $args);
        if ($selection) {
            $results = call_user_func_array(
                [$prep, 'fetchAll'],
                prep\ArgumentException::checkArgs([[7=>'style', 'argument', 'ctorArgs']], $args)
            );
            $prep->closeCursor();
        } else {
            $results = $prep;
        }

        if(!$results) {
            throw new prep\QueryFailedException(
                $selection ? 'MySQL returns an empty result' : 'No row has been modified'
            );
        }

        return $results;
    }

    /**
    * Only for selection : return a list of IDs from the result got from MySQL.
    * @param string $query --@see init--
    * @param array $args
        * @key 0 --same as init #0--
        * @key 1 --same as init #2--
        * @key 2 --same as init #3--
        * @key 3 --same as init #4--
        * @key 4 --same as init #5--
        * @key 5 --same as init #6--
        * @key 6:'fields'=> ~prep\Column The column you want to use to retrieve the IDs (default is 'ID')
        *                   (if an array is pass, you may select the right column in key #8 "argument")
        * @key 7..9 --@see all--
    * @return array
    * @throws prep\Exception If the query is not meant for selection
    */
    private static function ids ($query, $args)
    {
        if($query!=='select') {
            throw new prep\Exception('Unknown method');
        }

        $args = prep\ArgumentException::checkArgs(
            [
                ['table','where', 'join', 'orderBy', 'limit', 'fields', 'style', 'argument', 'ctorArgs'],
                array(),
                [5=>'ID', PDO::FETCH_COLUMN, 0]
            ],
            $args
        );

        try {
            return self::all($query, $args);
        } catch(Exception $e) {
            return [];
        }
    }

    /**
     * Only for selection : Count the number of rows in the result got from MySQL
     * @param string $query
     * @param array $args
         * @key 0:table=> prep\TableInterface
         * @key 1:where=> mixed
         * @key 2:group_by=> mixed
         * @key 3: rollup=> bool
         * @key 4:PDO=> PDO
     *
     * @return int|array
     * @throws prep\Exception If the query is not meant for selection
     */
    private static function count ($query, $args)
    {
        if ($query!='select') {
            throw new prep\Exception('Unknown method');
        }

        $args = prep\ArgumentException::checkArgs([['table', 'where', 'groupBy', 'rollup', 'PDO']], $args);
        if(isset($args['groupBy'])) {
            $args['fields'] = $args['groupBy'];
        }

        return self::init($query, $args)->count($args);
    }

    /**
     * Returns the name of the calling method, useful for the prep\Exception constructor
     * @return string
     */
    public static function getCallingMethodName()
    {
        return __CLASS__.'::'.self::$method_called;
    }
}
